﻿namespace kalkulatorUlamkow
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ProgressBar progressBar1;
            System.Windows.Forms.ProgressBar progressBar2;
            System.Windows.Forms.ProgressBar progressBar3;
            this.txbxMno1 = new System.Windows.Forms.MaskedTextBox();
            this.txbxLicz1 = new System.Windows.Forms.MaskedTextBox();
            this.txbxMian1 = new System.Windows.Forms.MaskedTextBox();
            this.txbxMian2 = new System.Windows.Forms.MaskedTextBox();
            this.txbxLicz2 = new System.Windows.Forms.MaskedTextBox();
            this.txbxMno2 = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txbxMianWyn = new System.Windows.Forms.MaskedTextBox();
            this.txbxLiczWyn = new System.Windows.Forms.MaskedTextBox();
            this.txbxMnoWyn = new System.Windows.Forms.MaskedTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            progressBar1 = new System.Windows.Forms.ProgressBar();
            progressBar2 = new System.Windows.Forms.ProgressBar();
            progressBar3 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            progressBar1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            progressBar1.ForeColor = System.Drawing.SystemColors.ControlText;
            progressBar1.Location = new System.Drawing.Point(83, 91);
            progressBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            progressBar1.Minimum = 100;
            progressBar1.Name = "progressBar1";
            progressBar1.Size = new System.Drawing.Size(100, 10);
            progressBar1.TabIndex = 3;
            progressBar1.Value = 100;
            // 
            // progressBar2
            // 
            progressBar2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            progressBar2.ForeColor = System.Drawing.SystemColors.ControlText;
            progressBar2.Location = new System.Drawing.Point(368, 91);
            progressBar2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            progressBar2.Minimum = 100;
            progressBar2.Name = "progressBar2";
            progressBar2.Size = new System.Drawing.Size(100, 10);
            progressBar2.TabIndex = 7;
            progressBar2.Value = 100;
            // 
            // progressBar3
            // 
            progressBar3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            progressBar3.ForeColor = System.Drawing.SystemColors.ControlText;
            progressBar3.Location = new System.Drawing.Point(613, 91);
            progressBar3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            progressBar3.Minimum = 100;
            progressBar3.Name = "progressBar3";
            progressBar3.Size = new System.Drawing.Size(100, 10);
            progressBar3.TabIndex = 14;
            progressBar3.Value = 100;
            // 
            // txbxMno1
            // 
            this.txbxMno1.Font = new System.Drawing.Font("Microsoft YaHei", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txbxMno1.Location = new System.Drawing.Point(12, 74);
            this.txbxMno1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txbxMno1.Name = "txbxMno1";
            this.txbxMno1.Size = new System.Drawing.Size(64, 43);
            this.txbxMno1.TabIndex = 1;
            this.txbxMno1.Text = "0";
            // 
            // txbxLicz1
            // 
            this.txbxLicz1.Font = new System.Drawing.Font("Microsoft YaHei", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txbxLicz1.Location = new System.Drawing.Point(99, 42);
            this.txbxLicz1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txbxLicz1.Name = "txbxLicz1";
            this.txbxLicz1.Size = new System.Drawing.Size(64, 43);
            this.txbxLicz1.TabIndex = 2;
            this.txbxLicz1.Text = "0";
            // 
            // txbxMian1
            // 
            this.txbxMian1.Font = new System.Drawing.Font("Microsoft YaHei", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txbxMian1.Location = new System.Drawing.Point(99, 107);
            this.txbxMian1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txbxMian1.Name = "txbxMian1";
            this.txbxMian1.Size = new System.Drawing.Size(64, 43);
            this.txbxMian1.TabIndex = 4;
            this.txbxMian1.Text = "1";
            // 
            // txbxMian2
            // 
            this.txbxMian2.Font = new System.Drawing.Font("Microsoft YaHei", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txbxMian2.Location = new System.Drawing.Point(384, 107);
            this.txbxMian2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txbxMian2.Name = "txbxMian2";
            this.txbxMian2.Size = new System.Drawing.Size(64, 43);
            this.txbxMian2.TabIndex = 8;
            this.txbxMian2.Text = "1";
            // 
            // txbxLicz2
            // 
            this.txbxLicz2.Font = new System.Drawing.Font("Microsoft YaHei", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txbxLicz2.Location = new System.Drawing.Point(384, 42);
            this.txbxLicz2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txbxLicz2.Name = "txbxLicz2";
            this.txbxLicz2.Size = new System.Drawing.Size(64, 43);
            this.txbxLicz2.TabIndex = 6;
            this.txbxLicz2.Text = "0";
            // 
            // txbxMno2
            // 
            this.txbxMno2.Font = new System.Drawing.Font("Microsoft YaHei", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txbxMno2.Location = new System.Drawing.Point(299, 74);
            this.txbxMno2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txbxMno2.Name = "txbxMno2";
            this.txbxMno2.Size = new System.Drawing.Size(64, 43);
            this.txbxMno2.TabIndex = 5;
            this.txbxMno2.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.label2.Location = new System.Drawing.Point(488, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 46);
            this.label2.TabIndex = 10;
            this.label2.Text = "=";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.label3.Location = new System.Drawing.Point(221, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 46);
            this.label3.TabIndex = 11;
            this.label3.Text = "?";
            // 
            // txbxMianWyn
            // 
            this.txbxMianWyn.Font = new System.Drawing.Font("Microsoft YaHei", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txbxMianWyn.Location = new System.Drawing.Point(629, 107);
            this.txbxMianWyn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txbxMianWyn.Name = "txbxMianWyn";
            this.txbxMianWyn.Size = new System.Drawing.Size(64, 43);
            this.txbxMianWyn.TabIndex = 15;
            // 
            // txbxLiczWyn
            // 
            this.txbxLiczWyn.Font = new System.Drawing.Font("Microsoft YaHei", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txbxLiczWyn.Location = new System.Drawing.Point(629, 42);
            this.txbxLiczWyn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txbxLiczWyn.Name = "txbxLiczWyn";
            this.txbxLiczWyn.Size = new System.Drawing.Size(64, 43);
            this.txbxLiczWyn.TabIndex = 13;
            // 
            // txbxMnoWyn
            // 
            this.txbxMnoWyn.Font = new System.Drawing.Font("Microsoft YaHei", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txbxMnoWyn.Location = new System.Drawing.Point(543, 74);
            this.txbxMnoWyn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txbxMnoWyn.Name = "txbxMnoWyn";
            this.txbxMnoWyn.Size = new System.Drawing.Size(64, 43);
            this.txbxMnoWyn.TabIndex = 12;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(45, 235);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(269, 63);
            this.button1.TabIndex = 16;
            this.button1.Text = "Dodaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(45, 338);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(269, 63);
            this.button4.TabIndex = 18;
            this.button4.Text = "Mnóż";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(423, 338);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(269, 63);
            this.button2.TabIndex = 20;
            this.button2.Text = "Dziel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(423, 235);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(269, 63);
            this.button3.TabIndex = 19;
            this.button3.Text = "Odejmij";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 450);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txbxMianWyn);
            this.Controls.Add(progressBar3);
            this.Controls.Add(this.txbxLiczWyn);
            this.Controls.Add(this.txbxMnoWyn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txbxMian2);
            this.Controls.Add(progressBar2);
            this.Controls.Add(this.txbxLicz2);
            this.Controls.Add(this.txbxMno2);
            this.Controls.Add(this.txbxMian1);
            this.Controls.Add(progressBar1);
            this.Controls.Add(this.txbxLicz1);
            this.Controls.Add(this.txbxMno1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox txbxMno1;
        private System.Windows.Forms.MaskedTextBox txbxLicz1;
        private System.Windows.Forms.MaskedTextBox txbxMian1;
        private System.Windows.Forms.MaskedTextBox txbxMian2;
        private System.Windows.Forms.MaskedTextBox txbxLicz2;
        private System.Windows.Forms.MaskedTextBox txbxMno2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox txbxMianWyn;
        private System.Windows.Forms.MaskedTextBox txbxLiczWyn;
        private System.Windows.Forms.MaskedTextBox txbxMnoWyn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}

