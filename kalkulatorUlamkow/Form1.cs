﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kalkulatorUlamkow
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        class Ulamek
        {
            public int licznik, mianownik = 1, mnoznik = 0;

            private int nwd(int a, int b)
            {
                return b == 0 ? a : nwd(b, a % b);
            }

            public Ulamek Uprosc()
            {
                licznik += mnoznik * mianownik;
                mnoznik = 0;
                return this;
            }

            public Ulamek Skroc()
            {

                while (licznik >= mianownik)
                {
                    mnoznik++;
                    licznik -= mianownik;
                }

                int nwd = this.nwd(licznik, mianownik);

                if (nwd == 1) return this;

                mianownik /= nwd;
                licznik /= nwd;
                return Skroc();
            }
        }

        private void WyslijWynik(Ulamek wynik)
        {
            txbxMnoWyn.Text = wynik.mnoznik.ToString();
            txbxLiczWyn.Text = wynik.licznik == 0 ? txbxLiczWyn.Text = "" : wynik.licznik.ToString();
            txbxMianWyn.Text = wynik.licznik == 0 ? txbxMianWyn.Text = "" : wynik.mianownik.ToString();
        }

        private Ulamek PobierzPierwszy()
        {
            pierwszy.licznik = int.Parse(txbxLicz1.Text);
            pierwszy.mianownik = int.Parse(txbxMian1.Text);
            pierwszy.mnoznik = int.Parse(txbxMno1.Text);

            return pierwszy;
        }

        private Ulamek PobierzDrugi()
        {
            drugi.licznik = int.Parse(txbxLicz2.Text);
            drugi.mianownik = int.Parse(txbxMian2.Text);
            drugi.mnoznik = int.Parse(txbxMno2.Text);

            return drugi;
        }

        Ulamek wynik = new Ulamek();
        Ulamek pierwszy = new Ulamek();
        Ulamek drugi = new Ulamek();

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PobierzPierwszy();
                PobierzDrugi();

                pierwszy.Uprosc();
                drugi.Uprosc();

                wynik.mianownik = pierwszy.mianownik * drugi.mianownik;
                wynik.licznik = pierwszy.licznik * (wynik.mianownik / pierwszy.mianownik) +
                                drugi.licznik * (wynik.mianownik / drugi.mianownik);
                wynik.mnoznik = 0;

                WyslijWynik(wynik.Skroc());
            }
            catch
            {
                MessageBox.Show("Popraw bledy!");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                PobierzPierwszy();
                PobierzDrugi();

                pierwszy.Uprosc();
                drugi.Uprosc();

                wynik.mianownik = pierwszy.mianownik * drugi.mianownik;
                wynik.licznik = pierwszy.licznik * (wynik.mianownik / pierwszy.mianownik) -
                                drugi.licznik * (wynik.mianownik / drugi.mianownik);
                wynik.mnoznik = 0;

                WyslijWynik(wynik.Skroc());
            }
            catch
            {
                MessageBox.Show("Popraw bledy!");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                PobierzPierwszy();
                PobierzDrugi();

                pierwszy.Uprosc();
                drugi.Uprosc();

                wynik.mianownik = pierwszy.mianownik * drugi.mianownik;
                wynik.licznik = pierwszy.licznik * drugi.licznik;
                wynik.mnoznik = 0;

                WyslijWynik(wynik.Skroc());
            }
            catch
            {
                MessageBox.Show("Popraw bledy!");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                PobierzPierwszy();
                PobierzDrugi();

                pierwszy.Uprosc();
                drugi.Uprosc();

                wynik.mianownik = pierwszy.mianownik * drugi.licznik;
                wynik.licznik = pierwszy.licznik * drugi.mianownik;
                wynik.mnoznik = 0;

                WyslijWynik(wynik.Skroc());
            }
            catch
            {
                MessageBox.Show("Popraw bledy!");
            }
        }
    }
}